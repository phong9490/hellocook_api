
<html>
<head>
    <meta http-equiv="Content-Type"  content="text/html charset=UTF-8" />
</head>
<body>
<h5>Thông báo mua hàng</h5>

<table id="index-box" class="table table-striped" border="1">
    <thead>
    <tr>
        <th colspan="7">Thông tin đơn hàng</th>

    </tr>
    <tr>
        <th >ID</th>
        <th >URL</th>
        <th >Kích thước</th>
        <th >Số lượng</th>
        <th >Giá</th>
        <th >Sai khác</th>
        <th >Thời gian</th>
    </tr>
    </thead>
    <tbody>

        @foreach($order_items as $order_item)
            <tr>
                <td>{{$order_item->id}}</td>
                <td>{{$order_item->url}}</td>
                <td>{{$order_item->request_size}}</td>
                <td>{{$order_item->request_quantity}}</td>
                <td>{{$order_item->request_price}}</td>
                <td>{{$order_item->request_variance}}</td>
                <td>{{$order_item->created_at}}</td>
            </tr>
        @endforeach

    </tbody>
</table>


</body>
</html>