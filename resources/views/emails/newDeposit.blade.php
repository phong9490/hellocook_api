
<html>
<head>
<meta http-equiv="Content-Type"  content="text/html charset=UTF-8" />
</head>
<body>
<h5>Yêu cầu đặt cọc</h5>
<table class="table cart"  border="1">
    <thead>
    <tr>
        <th colspan="2">Thông tin khách hàng</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <th>Họ và tên</th>
            <td>{{$payment->customer->user->last_name}} {{$payment->customer->user->first_name}}</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>{{$payment->customer->user->email}}</td>
        </tr>
        <tr>
            <th>Số điện thoại</th>
            <td>{{$payment->customer->user->phone}}</td>
        </tr>

    </tbody>
</table>
<br/>
<table id="index-box" class="table table-striped" border="1">
    <thead>
    <tr>
        <th colspan="2">Thông tin giá trị đặt cọc</th>

    </tr>
    <tr>
        <th >Giá trị</th>
        <th >Thời gian</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th >{{number_format($payment->amount)}}</th>
        <th >{{$payment->created_at}}</th>
    </tr>
    </tbody>
</table>
<br/>
<table id="index-box" class="table table-striped" border="1">
    <thead>
    <tr>
        <th colspan="7">Thông tin đơn hàng</th>

    </tr>
    <tr>
        <th >ID</th>
        <th >URL</th>
        <th >Kích thước</th>
        <th >Số lượng</th>
        <th >Giá</th>
        <th >Sai khác</th>
        <th >Thời gian</th>
    </tr>
    </thead>
    <tbody>
    @foreach($payment->invoice->orders as $order)
        @foreach($order->orderItems as $order_item)
    <tr>
        <td>{{$order_item->id}}</td>
        <td>{{$order_item->url}}</td>
        <td>{{$order_item->request_size}}</td>
        <td>{{$order_item->request_quantity}}</td>
        <td>{{$order_item->request_price}}</td>
        <td>{{$order_item->request_variance}}</td>
        <td>{{$order_item->created_at}}</td>
    </tr>
        @endforeach
     @endforeach
    </tbody>
</table>


</body>
</html>