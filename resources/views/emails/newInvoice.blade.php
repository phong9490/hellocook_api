
<html>
<head>
<meta http-equiv="Content-Type"  content="text/html charset=UTF-8" />
</head>
<body>
<h5>Yêu cầu đặt hàng</h5>
<table class="table cart"  border="1">
    <thead>
    <tr>
        <th colspan="2">Thông tin khách hàng</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <th>Họ và tên</th>
            <td>{{$invoice->customer->user->last_name}} {{$invoice->customer->user->first_name}}</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>{{$invoice->customer->user->email}}</td>
        </tr>
        <tr>
            <th>Số điện thoại</th>
            <td>{{$invoice->customer->user->phone}}</td>
        </tr>

    </tbody>
</table>
<br/>
<table id="index-box" class="table table-striped" border="1">
    <thead>
    <tr>
        <th colspan="7">Thông tin đơn hàng</th>

    </tr>
    <tr>
        <th >ID</th>
        <th >URL</th>
        <th >Kích thước</th>
        <th >Số lượng</th>
        <th >Giá</th>
        <th >Sai khác</th>
        <th >Thời gian</th>
    </tr>
    </thead>
    <tbody>
    @foreach($invoice->orders as $order)
        @foreach($order->orderItems as $order_item)
    <tr>
        <td>{{$order_item->id}}</td>
        <td>{{$order_item->url}}</td>
        <td>{{$order_item->request_size}}</td>
        <td>{{$order_item->request_quantity}}</td>
        <td>{{$order_item->request_price}}</td>
        <td>{{$order_item->request_variance}}</td>
        <td>{{$order_item->created_at}}</td>
    </tr>
        @endforeach
     @endforeach
    </tbody>
</table>


</body>
</html>