<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->string('contact')->nullable();
            $table->string('contact_phone');
            $table->string('address');
            $table->unsignedInteger('ward_id');
            $table->unsignedInteger('district_id');
            $table->unsignedInteger('province_id');
            $table->unsignedInteger('country_id');
            $table->timestamps();

            $table->foreign('customer_id')
                ->references('id')->on('customers')
                ->onDelete('cascade');
            $table->foreign('ward_id')
                ->references('id')->on('regions')
                ->onDelete('cascade');
            $table->foreign('district_id')
                ->references('id')->on('regions')
                ->onDelete('cascade');
            $table->foreign('province_id')
                ->references('id')->on('regions')
                ->onDelete('cascade');
            $table->foreign('country_id')
                ->references('id')->on('regions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_addresses');
    }
}
