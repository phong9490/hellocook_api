<?php

use App\Dish;
use Illuminate\Database\Seeder;

class DishSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dish = new Dish();
        $dish->name = 'Ba chỉ kho gừng';
        $dish->type = 1;
        $dish->save();

        $dish = new Dish();
        $dish->name = 'Tôm hấp';
        $dish->type = 2;
        $dish->save();

        $dish = new Dish();
        $dish->name = 'Muống luộc';
        $dish->type = 3;
        $dish->save();

        $dish = new Dish();
        $dish->name = 'Cải xào';
        $dish->type = 4;
        $dish->save();
    }
}
