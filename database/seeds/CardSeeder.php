<?php

use App\Card;
use Illuminate\Database\Seeder;

class CardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $card = new Card();
        $card->code = 'asdfasdf test card';
        $card->order_count = 20;
        $card->expire_duration = 40;
        $card->status = 1;
        $card->save();

        $card = new Card();
        $card->code = 'xcvbxcvb test card 2';
        $card->order_count = 40;
        $card->expire_duration = 60;
        $card->status = 1;
        $card->save();
    }
}
