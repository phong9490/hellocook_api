<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->last_name = 'Test';
        $user->first_name = 'Admin';
        $user->password = bcrypt('12345678');
        $user->phone = '0934652088';
        $user->email = 'admin@homefresh.vn';
        $user->save();
    }
}
