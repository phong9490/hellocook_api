<?php

use App\CustomerAddress;
use Illuminate\Database\Seeder;

class CustomerAddressSeeder extends Seeder
{
    public function run()
    {
        $address = new CustomerAddress();
        $address->customer_id = 1;
        $address->contact = 'asdasd';
        $address->contact_phone = '0987654321';
        $address->address = '76 hqv';
        $address->ward_id = 1102;
        $address->district_id = 176;
        $address->province_id = 4;
        $address->country_id = 1;
        $address->save();
    }
}
