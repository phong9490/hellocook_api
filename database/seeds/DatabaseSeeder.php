<?php
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RegionSeeder::class);
        $this->call(UserSeeder::class);
        // $this->call(CustomerAddressSeeder::class);
        $this->call(DishSeeder::class);
        $this->call(CardSeeder::class);
    }
}
