const prerender = require("prerender");
const memcache = require("prerender-memory-cache");

const server = prerender();
server.use(memcache);
server.start();
