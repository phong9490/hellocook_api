<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'type',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function dishBlacklist()
    {
        return $this->hasMany('App\CustomerDishBlacklist');
    }

    public function disease()
    {
        return $this->hasMany('App\CustomerDisease');
    }

    public function addresses()
    {
        return $this->hasMany('App\CustomerAddress');
    }

    public function order()
    {
        return $this->hasMany('App\Order');
    }

    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

    public function card()
    {
        return $this->hasManyDeep(
            'App\Card',
            ['App\CustomerCard'], // Intermediate models, beginning at the far parent (Customer).
            [
                'customer_id', // Foreign key on the "customerCard" table.
                'id', // Foreign key on the "customerCard" table.
            ],
            [
                'id', // Local key on the "customers" table.
                'card_id', // Local key on the "customerCard" table.
            ]);
    }
}
