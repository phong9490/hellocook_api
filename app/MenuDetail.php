<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuDetail extends Model
{
    public function dish()
    {
        return $this->belongsTo('App\Dish', 'dish_id');
    }

    public function menu()
    {
        return $this->belongsTo('App\Menu', 'menu_id');
    }
}
