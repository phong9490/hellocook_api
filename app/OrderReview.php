<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderReview extends Model
{
    protected $fillable = [
        'rating',
        'comment',
    ];

    public function order()
    {
        return $this->belongsTo('App\OrderDetail', 'order_id');
    }
}
