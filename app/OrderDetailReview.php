<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetailReview extends Model
{
    protected $fillable = [
        'rating',
        'comment',
    ];

    public function orderDetail()
    {
        return $this->belongsTo('App\OrderDetail', 'order_detail_id');
    }
}
