<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function ward()
    {
        return $this->belongsTo('App\Region', 'ward_id');
    }

    public function district()
    {
        return $this->belongsTo('App\Region', 'district_id');
    }

    public function province()
    {
        return $this->belongsTo('App\Region', 'province_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Region', 'country_id');
    }
}
