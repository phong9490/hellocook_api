<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    public function customerCard()
    {
        return $this->hasOne('App\CustomerCard');
    }
}
