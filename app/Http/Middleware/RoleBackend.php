<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class RoleBackend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (in_array(Auth::user()->role, [User::ROLE_ADMIN, User::ROLE_BUYER])) {
            return $next($request);
        }
        return response()->json([
            'error' => 'Permission denied',
        ], 403);
    }
}
