<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function send(Request $request)
    {
        /*//save to order
        $format = 'd/m/Y H:i';

        $order = new Order();
        $order->name = $request->contact_name;
        $order->email = $request->contact_email;

        $order->phone = $request->contact_phone;
        $order->note = $request->contact_message;


        //send mail*/


        $data = array(
            'order' => $request,

        );

        Mail::send('emails.contact', $data, function($message)
        {   $message->from("admin@youngpilots.vn");
            $message->to('gondt8@gmail.com')->subject('Liên Hệ ');
        });

        return response()->json($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $order = Order::findOrFail($id);
        $categories = Order::all();

        return view('order.edit', ['order' => $order, 'parents' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        $order = Order::findOrFail($id);

        $order->delete();

        return redirect()->route('admin.order.index');
    }
}
