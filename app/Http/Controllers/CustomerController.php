<?php

namespace App\Http\Controllers;

use App\Card;
use App\User;
use App\Customer;
use App\CustomerCard;
use App\CustomerAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $query_builder = Customer::with('user', 'card', 'order', 'addresses')
        ->whereHas('user', function ($query) {
            $query->where('role', 2);
        });

        // get sorting
        $sortings = $request->get('sortings');
        if (isset($sortings)) {
            $sortings = json_decode($sortings);
            foreach ($sortings as $index => $value) {
                $query_builder->orderBy($index, $value);
            }
        } else {
            $query_builder->orderBy('id', 'ASC');
        }

        // get filters
        $filters = $request->get('filters');
        if (isset($filters)) {
            $filters = json_decode($filters);
            foreach ($filters as $index => $value) {
                if ($index === 'keyword') {
                    $query_builder->whereHas('user', function ($query) use ($value) {
                        $query->whereRaw("CONCAT(last_name, ' ', first_name, ' ', phone, ' ', email) like '%$value%'");
                    });
                } else {
                    $query_builder->where($index, $value);
                }
            }
        }

        // get page size
        $limit = (int) $request->get('limit', 20);
        $customers = $query_builder->paginate($limit);

        return response()->json($customers);
    }

    public function detail()
    {
        $customer = Customer::with('user', 'card', 'order')->findOrFail(Auth::user()->customer->id);

        return response()->json($customer);
    }

    public function detailCard()
    {
        $card = CustomerCard::with('cardInfo')
        ->where('customer_id', Auth::user()->customer->id)
        ->where('status', 1)
        ->get();

        return response()->json($card);
    }

    public function registerCard(Request $request)
    {
        $rules = [
            'province_id' => 'required|numeric',
            'district_id' => 'required|numeric',
            'ward_id' => 'required|numeric',
            'address' => 'required|string|max:255',
            'contact' => 'required|string|max:255',
            'contact_phone' => 'required|string|max:255',
            'code' => 'required|string|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }

        $cardInfo = Card::where('code', '=', $request->get('code'))->where('status', '=', 1)->first();
        if (!empty($cardInfo)) {
            $customerCard = CustomerCard::where('card_id', $cardInfo->id)->first();

            if ($customerCard) {
                return response()->json([
                    'error' => 'Mã kích hoạt đã được sử dụng',
                ], 403);
            }

            return DB::transaction(function () use ($cardInfo, $request) {
                $address = CustomerAddress::where('contact_phone', $request->get('contact_phone'))
                ->where('customer_id', Auth::user()->customer->id)
                ->where('district_id', $request->get('district_id'))
                ->where('province_id', $request->get('province_id'))
                ->where('ward_id', $request->get('ward_id'))->first();

                if(!$address) {
                    $address = new CustomerAddress();
                    $address->customer_id = Auth::user()->customer->id;
                    $address->country_id = 1;
                    $address->province_id = $request->get('province_id');
                    $address->district_id = $request->get('district_id');
                    $address->ward_id = $request->get('ward_id');
                    $address->contact = $request->get('contact');
                    $address->contact_phone = $request->get('contact_phone');
                    $address->address = $request->get('address');
                    $address->save();
                }

                $newCustomerCard = new CustomerCard();
                $newCustomerCard->customer_id = Auth::user()->customer->id;
                $newCustomerCard->card_id = $cardInfo->id;
                $newCustomerCard->order_count = $cardInfo->order_count;
                $newCustomerCard->status = 1;
                $newCustomerCard->start_date = date('Y-m-d');
                $newCustomerCard->end_date = date('Y-m-d', strtotime("+" . $cardInfo->expire_duration . " days"));
                $newCustomerCard->customer_address_id = $address->id;
                $newCustomerCard->save();

                $cardInfo->status = 0;
                $cardInfo->save();

                return response()->json([
                    'customerCard' => $newCustomerCard,
                ]);
            });
        } else {
            return response()->json([
                'errors' => ['code' => 'Mã kích hoạt không hợp lệ'],
            ], 403);
        }
    }

    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);

        $rules = [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . $user->id,
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->phone = $request->get('phone');
        $user->email = $request->get('email');
        // $user->birthday = date("Y-m-d", strtotime($request->get('birthday')));
        $user->save();

        $customer = Customer::where('user_id', $user->id)->first();
        $customer->diseases = $request->get('diseases');
        $customer->back_list = $request->get('back_list');
        $customer->save();

        return response()->json([
            'user' => $user,
        ]);

    }


    public function changePassword(Request $request)
    {
        $user = Auth::user();

        $rules = [
            'password' => 'required|string|max:255',
            'password_confirm' => 'required|string|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }

        if($request->get('password') !== $request->get('password_confirm')) {
            return response()->json([
                'errors' => ['password_confirm' => 'NOT MATCH'],
            ], 422);
        }

        if(bcrypt($request->get('password')) === $user->password) {
            return response()->json([
                'errors' => ['password' => 'Used password'],
            ], 422);
        }

        $user->password = bcrypt($request->get('password'));
        $user->save();

        return response()->json([
            'user' => $user,
        ]);
    }

    public function addresses($id)
    {
        $addresses = CustomerAddress::with('ward', 'district', 'province', 'country')->where('customer_id', $id)->get();

        return response()->json([
            'addresses' => $addresses,
        ]);
    }    
}
