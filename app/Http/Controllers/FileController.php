<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Validator;

class FileController extends Controller
{

    public function upload(Request $request)
    {
        /*
         * validate file input
         */
        $rules = [
            'file' => 'required|image|mimes:jpeg,png,ipg,gif|max:2048',

        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        /*
         * save image and return Url path
         */
        $image = $request->file('file');

        $new_name = Uuid::uuid4() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('uploads'), $new_name);
        $url_path = url('/') . '/uploads' . '/' . $new_name;
        return response()->json([
            'url' => $url_path,
        ]);
    }
}
