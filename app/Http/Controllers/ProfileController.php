<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function update(Request $request)
    {
        $user = Auth::user();

        $rules = [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'required|email|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->phone = $request->get('phone');
        $user->email = $request->get('email');
        $user->save();

        return response()->json([
            'user' => $user,
        ]);
    }

    public function changePassword(Request $request)
    {
        $user = Auth::user();
        $rules = [
            'current_password' => 'required|string|max:255',
            'new_password' => 'required|string|max:255|min:8',
            'confirm_password' => 'required_with:new_password|same:new_password',

        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        if (!Hash::check($request->current_password, $user->password)) {
            return response()->json([
                'errors' => ['current_password' => 'Wrong current password.'],
            ], 422);
        }
        $user->password = bcrypt($request->new_password);
        $user->save();

        return response()->json([
            'user' => $user,
        ]);
    }
}
