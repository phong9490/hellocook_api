<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $rules = [
            'phone' => 'required|string|max:255',
            'password' => 'required|string|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        } else {
            $phone = $request->get('phone');
            $password = $request->get('password');

            if (Auth::attempt([
                'phone' => $phone,
                'password' => $password,
            ])) {
                $user = Auth::user();

                $token = $user->createToken(env('APP_NAME', 'ORDER-API'))->accessToken;

                return response()->json([
                    'user' => $user,
                    'token' => $token,
                ]);
            } else {
                $user = User::where('phone', $request->input('phone'))->first();

                if ($user) {
                    return response()->json([
                        'errors' => [
                            'password' => 'INVALID_PASSWORD',
                        ],
                    ], 401);
                }

                return response()->json([
                    'errors' => [
                        'phone' => 'INVALID_PHONE',
                    ],
                ], 401);
            }
        }
    }

    public function loginCms(Request $request)
    {
        $rules = [
            'email' => 'required|string|max:255',
            'password' => 'required|string|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        } else {
            $phone = $request->get('email');
            $password = $request->get('password');

            if (Auth::attempt([
                'email' => $phone,
                'password' => $password,
            ])) {
                $user = Auth::user();

                $token = $user->createToken(env('APP_NAME', 'ORDER-API'))->accessToken;

                return response()->json([
                    'user' => $user,
                    'token' => $token,
                ]);
            } else {
                $user = User::where('email', $request->input('email'))->first();

                if ($user) {
                    return response()->json([
                        'errors' => [
                            'email' => 'INVALID_EMAIL',
                        ],
                    ], 401);
                }

                return response()->json([
                    'errors' => [
                        'password' => 'INVALID_PASSWORD',
                    ],
                ], 401);
            }
        }
    }

    public function register(Request $request)
    {
        $rules = [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'phone' => 'required|string|max:12|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|string|max:255',
            'role' => 'numeric',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        return DB::transaction(function () use ($request) {
            $user = new User();
            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->phone = $request->get('phone');
            $user->email = $request->get('email');
            $user->role = $request->get('role');
            $user->birthday = gmdate("Y-m-d H:i:s", $request->get('birthday'));
            $user->password = bcrypt($request->get('password'));
            $user->save();

            $token = $user->createToken(env('APP_NAME', 'ORDER-API'))->accessToken;

            return response()->json([
                'user' => $user,
                'token' => $token,
            ]);
        });
    }
}
