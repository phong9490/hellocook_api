<?php

namespace App\Http\Controllers;

use App\Card;
use App\CustomerCard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Customer;

class CardController extends Controller
{
    public function index(Request $request)
    {
        $query_builder = Card::with('customerCard')->whereRaw('1 = 1');

        // get sorting
        $sortings = $request->get('sortings');
        if (isset($sortings)) {
            $sortings = json_decode($sortings);
            foreach ($sortings as $index => $value) {
                $query_builder->orderBy($index, $value);
            }
        } else {
            $query_builder->orderBy('id', 'ASC');
        }

        // get filters
        $filters = $request->get('filters');
        if (isset($filters)) {
            $filters = json_decode($filters);
            foreach ($filters as $index => $value) {
                if ($index === 'keyword') {
                    $query_builder->whereRaw("CONCAT(code, ' ', order_count, ' ', expire_duration) like '%$value%'");
                } else {
                    $query_builder->where($index, $value);
                }
            }
        }

        // get page size
        $limit = (int)$request->get('limit', 20);
        $cards = $query_builder->paginate($limit);

        return response()->json($cards);
    }

    public function store(Request $request)
    {
        $rules = [
            'code' => 'required|string|max:255',
            'order_count' => 'required|numeric|min:0',
            'expire_duration' => 'required|numeric|min:0',
            'status' => 'required|numeric|min:0',
            'type' => 'required|numeric|min:1|max:2',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        return DB::transaction(function () use ($request) {
            $card = new Card();
            $card->code = $request->get('code');
            $card->order_count = $request->get('order_count');
            $card->expire_duration = $request->get('expire_duration');
            $card->status = $request->get('status');
            $card->type = $request->get('type');
            $card->save();

            return response()->json([
                'card' => $card,
            ]);
        });
    }

    public function update($id, Request $request)
    {
        $card = Card::findOrFail($id);

        $rules = [
            'order_count' => 'required|numeric|min:0',
            'expire_duration' => 'required|numeric|min:0',
            'status' => 'required|numeric|min:0',
            'type' => 'required|numeric|min:1|max:2',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        $card->order_count = $request->get('order_count');
        $card->expire_duration = $request->get('expire_duration');
        $card->status = $request->get('status');
        $card->type = $request->get('type');
        $card->save();

        return response()->json([
            'card' => $card,
        ]);
    }

    public function listById($id, Request $request)
    {
        $cards = CustomerCard::with('cardInfo')
            ->where('customer_id', $id)
            ->where('status', 1)
            ->get();

        return response()->json($cards);
    }

    public function destroy($id)
    {
        $card = Card::findOrFail($id);

        $card->delete();

        return response()->json([
            'card' => $card,
        ]);
    }
}
