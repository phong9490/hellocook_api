<?php

namespace App\Http\Controllers;

use App\Dish;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DishController extends Controller
{
    public function index(Request $request)
    {
        $query_builder = Dish::whereRaw('1 = 1');

        // get sorting
        $sortings = $request->get('sortings');
        if (isset($sortings)) {
            $sortings = json_decode($sortings);
            foreach ($sortings as $index => $value) {
                $query_builder->orderBy($index, $value);
            }
        } else {
            $query_builder->orderBy('id', 'ASC');
        }

        // get filters
        $filters = $request->get('filters');
        if (isset($filters)) {
            $filters = json_decode($filters);
            foreach ($filters as $index => $value) {
                if ($index === 'keyword') {
                    $query_builder->whereRaw("name like '%$value%'");
                } else {
                    $query_builder->where($index, $value);
                }
            }
        }

        // get page size
        $limit = (int) $request->get('limit', 20);
        $dishes = $query_builder->paginate($limit);

        return response()->json($dishes);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'type' => 'required|numeric',
            'img' => 'string|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        return DB::transaction(function () use ($request) {
            $dish = new Dish();
            $dish->name = $request->get('name');
            $dish->type = $request->get('type');
            $dish->img = $request->get('img');
            $dish->save();

            return response()->json([
                'dish' => $dish,
            ]);
        });
    }

    public function update($id, Request $request)
    {
        $dish = Dish::findOrFail($id);

        $rules = [
            'name' => 'required|string|max:255',
            'type' => 'required|numeric',
            'img' => 'string|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        $dish->name = $request->get('name');
        $dish->type = $request->get('type');
        $dish->img = $request->get('img');
        $dish->save();

        return response()->json([
            'dish' => $dish,
        ]);

    }

    public function destroy($id)
    {
        $dish = Dish::findOrFail($id);

        $dish->delete();

        return response()->json([
            'dish' => $dish,
        ]);
    }

}
