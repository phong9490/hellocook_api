<?php

namespace App\Http\Controllers;

use App\Menu;
use App\MenuDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MenuController extends Controller
{
    public function index(Request $request)
    {
        $query_builder = Menu::with('details')->whereRaw('1 = 1');

        // get sorting
        $sortings = $request->get('sortings');
        if (isset($sortings)) {
            $sortings = json_decode($sortings);
            foreach ($sortings as $index => $value) {
                $query_builder->orderBy($index, $value);
            }
        } else {
            $query_builder->orderBy('id', 'ASC');
        }

        // get filters
        $filters = $request->get('filters');
        if (isset($filters)) {
            $filters = json_decode($filters);
            foreach ($filters as $index => $value) {
                if ($index === "date") {
                    if (isset($value->startDate)) {
                        $query_builder->whereRaw("day >= STR_TO_DATE('$value->startDate', '%d-%m-%Y')");
                    }
                    if (isset($value->endDate)) {
                        $query_builder->whereRaw("day <= STR_TO_DATE('$value->endDate', '%d-%m-%Y')");
                    }
                } else {
                    $query_builder->where($index, $value);
                }
            }
        }

        // get page size
        $limit = (int) $request->get('limit', 20);
        $cards = $query_builder->paginate($limit);

        return response()->json($cards);
    }

    public function menuByDay($day, Request $request)
    {
        $menu = Menu::with('details')->where('day', date("Y-m-d", strtotime($day)))->get();
        return response()->json($menu);
    }

    public function store(Request $request)
    {
        $rules = [
            'day' => 'required|date_format:d-m-Y',
            'type' => 'required|numeric',
            'details' => 'required',
            'size' => 'required|numeric',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        return DB::transaction(function () use ($request) {
            $menu = new Menu();
            $menu->day = date("Y-m-d", strtotime($request->get('day')));
            $menu->type = $request->get('type');
            $menu->size = $request->get('size');
            $menu->save();

            $details = $request->get('details');
            $menu_id = $menu->id;
            foreach ($details as $index => $detail) {
                $menu_detail = new MenuDetail();
                $menu_detail->menu_id = $menu_id;
                $menu_detail->dish_id = $detail['id'];
                $menu_detail->type = $index + 1;
                $menu_detail->save();
            }

            return response()->json([
                'menu' => $menu,
            ]);
        });
    }

    public function update($id, Request $request)
    {
        $menu = Menu::findOrFail($id);

        $rules = [
            'day' => 'required|date_format:d-m-Y',
            'type' => 'required|numeric',
            'size' => 'required|numeric',
            'details' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        $menu->day = date("Y-m-d", strtotime($request->get('day')));
        $menu->type = $request->get('type');
        $menu->size = $request->get('size');
        $menu->save();
        $details = $request->get('details');

        foreach ($details as $index => $detail) {
            if (isset($detail['menu_detail_id'])) {
                // update menu detail
                $menu_detail = MenuDetail::findOrFail($id);
                $menu_detail->dish_id = $detail['id'];
                $menu_detail->type = $index + 1;
                $menu_detail->save();
            } else {
                // create menu detail
                $menu_detail = new MenuDetail();
                $menu_detail->menu_id = $id;
                $menu_detail->dish_id = $detail['id'];
                $menu_detail->type = $index + 1;
                $menu_detail->save();
            }
        }

        return response()->json([
            'menu' => $menu,
        ]);

    }

    public function destroy($id)
    {
        $menu = Menu::findOrFail($id);

        $menu->delete();

        return response()->json([
            'menu' => $menu,
        ]);
    }
}
