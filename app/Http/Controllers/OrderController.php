<?php

namespace App\Http\Controllers;

use App\CustomerAddress;
use App\CustomerCard;
use App\Order;
use App\OrderDetailReview;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $query_builder = Order::with(
            'menu',
            'review',
            'detail',
            'delivery',
            'delivery.ward',
            'delivery.district',
            'delivery.province',
            'delivery.country'
        )
            ->where('customer_id', Auth::user()->customer->id);

        // get sorting
        $sortings = $request->get('sortings');
        if (isset($sortings)) {
            $sortings = json_decode($sortings);
            foreach ($sortings as $index => $value) {
                $query_builder->orderBy($index, $value);
            }
        } else {
            $query_builder->orderBy('id', 'ASC');
        }

        if ($request->get('day')) {
            $query_builder->where('day', $request->get('day'));
        }

        // get filters
        $filters = $request->get('filters');
        if (isset($filters)) {
            $filters = json_decode($filters);
            foreach ($filters as $index => $value) {
                if ($index === "date") {
                    if (isset($value->startDate)) {
                        $query_builder->whereRaw("created_at >= STR_TO_DATE('$value->startDate', '%d-%m-%Y')");
                    }
                    if (isset($value->endDate)) {
                        $query_builder->whereRaw("created_at <= STR_TO_DATE('$value->endDate', '%d-%m-%Y')");
                    }
                } else {
                    $query_builder->where($index, $value);
                }
            }
        }

        // get page size
        $limit = (int)$request->get('limit', 20);
        $orders = $query_builder->paginate($limit);

        return response()->json($orders);
    }

    public function listOrders(Request $request)
    {
        $query_builder = Order::with(
            'menu',
            'review',
            'delivery',
            'delivery.ward',
            'delivery.district',
            'delivery.province',
            'delivery.country'
        );

        // get sorting
        $sortings = $request->get('sortings');
        if (isset($sortings)) {
            $sortings = json_decode($sortings);
            foreach ($sortings as $index => $value) {
                $query_builder->orderBy($index, $value);
            }
        } else {
            $query_builder->orderBy('id', 'ASC');
        }

        // get filters
        $filters = $request->get('filters');
        if (isset($filters)) {
            $filters = json_decode($filters);
            foreach ($filters as $index => $value) {
                if ($index === "date") {
                    if (isset($value->startDate)) {
                        $query_builder->whereRaw("created_at >= STR_TO_DATE('$value->startDate', '%d-%m-%Y')");
                    }
                    if (isset($value->endDate)) {
                        $query_builder->whereRaw("created_at <= STR_TO_DATE('$value->endDate', '%d-%m-%Y')");
                    }
                } else {
                    $query_builder->where($index, $value);
                }
            }
        }

        // get page size
        $limit = (int)$request->get('limit', 20);
        $orders = $query_builder->paginate($limit);

        return response()->json($orders);
    }

    public function listReviews(Request $request)
    {
        $query_builder = Order::with('review', 'detail', 'detailReview', 'menu');

        // get sorting
        $sortings = $request->get('sortings');
        if (isset($sortings)) {
            $sortings = json_decode($sortings);
            foreach ($sortings as $index => $value) {
                $query_builder->orderBy($index, $value);
            }
        } else {
            $query_builder->orderBy('id', 'ASC');
        }

        // get filters
        $filters = $request->get('filters');
        if (isset($filters)) {
            $filters = json_decode($filters);
            foreach ($filters as $index => $value) {
                if ($index === "date") {
                    if (isset($value->startDate)) {
                        $query_builder->whereRaw("created_at >= STR_TO_DATE('$value->startDate', '%d-%m-%Y')");
                    }
                    if (isset($value->endDate)) {
                        $query_builder->whereRaw("created_at <= STR_TO_DATE('$value->endDate', '%d-%m-%Y')");
                    }
                } else {
                    $query_builder->where($index, $value);
                }
            }
        }

        // get page size
        $limit = (int)$request->get('limit', 20);
        $orders = $query_builder->paginate($limit);

        return response()->json($orders);
    }

    public function update(Request $request, $id)
    {
        /*
         * only use by Admin
         */

        $this->validate($request, [
            'address' => 'required|string|max:255',
            'contact' => 'required|string|max:255',
            'contact_phone' => 'required|string|max:255',
            'customer_id' => 'required|numeric|min:1',
            'delivery_from' => 'required|numeric|min:360|max:1320',
            'delivery_to' => 'required|numeric|min:360|max:1320',
            'district_id' => 'required|numeric|min:2',
            'province_id' => 'required|numeric|min:2',
            'ward_id' => 'required|numeric|min:5',
            'menu_id' => 'required|numeric|min:1',
            'customer_card_id' => 'required|numeric|min:1',
        ]);

        $order = Order::findOrFail($id);

        return DB::transaction(function () use ($request, $order) {
            $address = CustomerAddress::findOrFail($request->get('customer_address_id'));
            $address->country_id = 1;
            $address->province_id = $request->get('province_id');
            $address->district_id = $request->get('district_id');
            $address->ward_id = $request->get('ward_id');
            $address->contact = $request->get('contact');
            $address->contact_phone = $request->get('contact_phone');
            $address->address = $request->get('address');
            $address->save();

            $order->contact = $request->get('contact');
            $order->contact_phone = $request->get('contact_phone');
            $order->customer_id = $request->get('customer_id');
            $order->delivery_from = $request->get('delivery_from');
            $order->delivery_to = $request->get('delivery_to');
            $order->customer_card_id = $request->get('customer_card_id');
            $order->menu_id = $request->get('menu_id');
            $order->save();

            return response()->json($order);
        });
    }

    public function store(Request $request)
    {
        $rules = [
            'address' => 'required|string|max:255',
            'contact' => 'required|string|max:255',
            'contact_phone' => 'required|string|max:255',
            'customer_id' => 'required|numeric|min:1',
            'delivery_from' => 'required|numeric|min:360|max:1320',
            'delivery_to' => 'required|numeric|min:360|max:1320',
            'district_id' => 'required|numeric|min:2',
            'province_id' => 'required|numeric|min:2',
            'ward_id' => 'required|numeric|min:5',
            'menu_id' => 'required|numeric|min:1',
            'customer_card_id' => 'required|numeric|min:1',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }

        return DB::transaction(function () use ($request) {
            $address = CustomerAddress::where('contact_phone', $request->get('contact_phone'))
                ->where('customer_id', Auth::user()->customer->id)
                ->where('district_id', $request->get('district_id'))
                ->where('province_id', $request->get('province_id'))
                ->where('ward_id', $request->get('ward_id'))->first();

            if (!$address) {
                $address = new CustomerAddress();
                $address->customer_id = Auth::user()->customer->id;
                $address->country_id = 1;
                $address->province_id = $request->get('province_id');
                $address->district_id = $request->get('district_id');
                $address->ward_id = $request->get('ward_id');
                $address->contact = $request->get('contact');
                $address->contact_phone = $request->get('contact_phone');
                $address->address = $request->get('address');
                $address->save();
            }

            $order = new Order();
            $order->contact = $request->get('contact');
            $order->contact_phone = $request->get('contact_phone');
            $order->customer_id = $request->get('customer_id');
            $order->delivery_from = $request->get('delivery_from');
            $order->delivery_to = $request->get('delivery_to');
            $order->customer_card_id = $request->get('customer_card_id');
            $order->customer_address_id = $address->id;
            $order->menu_id = $request->get('menu_id');
            $order->save();

            return response()->json($order);
        });
    }

    public function listRating(Request $request)
    {
        $query_builder = Order::with(
            'review',
            'menu',
            'detail',
            'detailReview',
            'delivery',
            'delivery.ward',
            'delivery.district',
            'delivery.province',
            'delivery.country'
        )
            ->where('customer_id', Auth::user()->customer->id);

        // get filters
        $filters = $request->get('filters');
        if (isset($filters)) {
            $filters = json_decode($filters);
            foreach ($filters as $index => $value) {
                $query_builder->where($index, $value);
            }
        }

        if ($request->get('start_date')) {
            $date = $request->get('start_date');
            $query_builder->whereRaw("day >= STR_TO_DATE('$date', '%d/%m/%Y')");
        }
        if ($request->get('end_date')) {
            $date = $request->get('end_date');
            $query_builder->whereRaw("day <= STR_TO_DATE('$date', '%d/%m/%Y')");
        }

        // get page size
        $limit = (int)$request->get('limit', 20);
        $orders = $query_builder->paginate($limit);

        return response()->json($orders);
    }

    public function review(Request $request)
    {
        $rules = [
            'details' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }

        $details = $request->get('details');
        foreach ($details as $detail) {
            $orderDetailReview = OrderDetailReview::where('order_detail_id', $detail['order_detail_id'])->first();
            if ($orderDetailReview) {
                $orderDetailReview->rating = $detail['rating'];
                $orderDetailReview->save();
            } else {
                $orderDetailReview = new OrderDetailReview();
                $orderDetailReview->order_detail_id = $detail['order_detail_id'];
                $orderDetailReview->rating = $detail['rating'];
                $orderDetailReview->save();
            }
        }

        return response()->json($details);
    }

    public function detail($id)
    {
        $order = Order::with(
            'detail',
            'delivery',
            'delivery.ward',
            'delivery.district',
            'delivery.province',
            'delivery.country'
        )->findOrFail($id);

        return response()->json($order);
    }

    public function cancelAdmin($id)
    {
        $order = Order::findOrFail($id);
        $order->status = 0;
        $order->save();
        return response()->json($order);
    }

    public function cancelOrder($id)
    {
        $order = Order::findOrFail($id);

        $order_date = Carbon::createFromFormat('Y-m-d', $order->menu->day)->startOfDay();
        $now = Carbon::today();

        if ($order_date->diffInHours($now) < 8) {
            return response()->json(['errors' => ['Đã quá giờ hủy đơn. Đơn hàng chỉ được hủy trước 16:00!']], 422);
        }

        $order->status = 0;
        $order->save();
        return response()->json($order);
    }
}
