<?php

namespace App\Http\Controllers;

use App\Region;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    public function index(Request $request)
    {
        $query_builder = Region::whereRaw('1 = 1');

        // get sorting
        $sortings = $request->get('sortings');
        if (isset($sortings)) {
            $sortings = json_decode($sortings);
            foreach ($sortings as $index => $value) {
                $query_builder->orderBy($index, $value);
            }
        } else {
            $query_builder->orderBy('id', 'ASC');
        }

        // get filters
        $filters = $request->get('filters');
        if (isset($filters)) {
            $filters = json_decode($filters);
            foreach ($filters as $index => $value) {
                if ($index === 'keyword') {
                    $query_builder->whereRaw("name like '%$value%'");
                } else {
                    $query_builder->where($index, $value);
                }
            }
        }

        // get page size
        $limit = (int) $request->get('limit', 20);
        $regions = $query_builder->paginate($limit);

        return response()->json($regions);
    }
}
