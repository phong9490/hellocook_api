<?php

namespace App\Http\Controllers;

use App\CustomerAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CustomerAddressController extends Controller
{
    public function index($id, Request $request)
    {
        $query_builder = CustomerAddress::with(
            'customer',
            'customer.user',
            'ward',
            'district',
            'province'
        )->where('customer_id', $id);

        // get sorting
        $sortings = $request->get('sortings');
        if (isset($sortings)) {
            $sortings = json_decode($sortings);
            foreach ($sortings as $index => $value) {
                $query_builder->orderBy($index, $value);
            }
        } else {
            $query_builder->orderBy('id', 'ASC');
        }

        // get filters
        $filters = $request->get('filters');
        if (isset($filters)) {
            $filters = json_decode($filters);
            foreach ($filters as $index => $value) {
                if ($index === "date") {
                    if (isset($value->startDate)) {
                        $query_builder->whereRaw("created_at >= STR_TO_DATE('$value->startDate', '%d-%m-%Y')");
                    }
                    if (isset($value->endDate)) {
                        $query_builder->whereRaw("created_at <= STR_TO_DATE('$value->endDate', '%d-%m-%Y')");
                    }
                } else {
                    $query_builder->where($index, $value);
                }
            }
        }

        $deliveries = $query_builder->get();

        return response()->json($deliveries);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'delivery_country_id' => 'required|numeric',
            'delivery_province_id' => 'required|numeric',
            'delivery_district_id' => 'required|numeric',
            'delivery_ward_id' => 'required|numeric',
            'contact' => 'required|string|max:255',
            'contact_phone' => 'required|string|max:255',
            'address' => 'required|string|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }

        $delivery = CustomerAddress::findOrFail($id);
        $delivery->province_id = $request->get('delivery_province_id');
        $delivery->district_id = $request->get('delivery_district_id');
        $delivery->ward_id = $request->get('delivery_ward_id');
        $delivery->contact = $request->get('contact');
        $delivery->contact_phone = $request->get('contact_phone');
        $delivery->address = $request->get('address');
        $delivery->save();

        return response()->json([
            'delivery' => $delivery,
        ]);
    }

    public function store(Request $request)
    {
        $rules = [
            'delivery_country_id' => 'required|numeric',
            'delivery_province_id' => 'required|numeric',
            'delivery_district_id' => 'required|numeric',
            'delivery_ward_id' => 'required|numeric',
            'contact' => 'required|string|max:255',
            'contact_phone' => 'required|string|max:255',
            'address' => 'required|string|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }

        return DB::transaction(function () use ($request) {
            $address = new CustomerAddress();
            $address->customer_id = Auth::user()->customer->id;
            $address->country_id = $request->get('delivery_country_id');
            $address->province_id = $request->get('delivery_province_id');
            $address->district_id = $request->get('delivery_district_id');
            $address->ward_id = $request->get('delivery_ward_id');
            $address->contact = $request->get('contact');
            $address->contact_phone = $request->get('contact_phone');
            $address->address = $request->get('address');
            $address->save();

            return response()->json([
                'addresses' => $address,
            ]);
        });
    }

    public function show($id)
    {
        $order = CustomerAddress::with('orderItems', 'orderItems.currency')->findOrFail($id);

        return response()->json([
            'delivery' => $order,
        ]);
    }
}
