<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerDisease extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'diseases_id',
    ];

    public function disease()
    {
        return $this->belongsTo('App\Disease', 'diseases_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }
}
