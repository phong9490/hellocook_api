<?php

namespace App\Console\Commands;

use App\CustomerCard;
use App\Menu;
use App\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CreateOrderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create order daily at 0:00';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cards = CustomerCard::with('address', 'customer')
            ->where('status', 1)
            ->where('order_count', '>=', 1)
            ->where('start_date', '<=', Carbon::today()->toDateString())
            ->where('end_date', '>=', Carbon::today()->toDateString())
            ->get();

        $menus = Menu::where('day', Carbon::today()->toDateString())->orderBy('type', 'ASC')->get();

        if (isset($menus)) {
            $totalMenu = sizeof($menus);

            foreach ($cards as $card) {
                $menuType = $menus[0];
                if ((isset($card->customer->diseases) || isset($card->customer->back_list)) && $totalMenu >= 2) {
                    $menuType = $menus[1];
                }

                $order = new Order();
                $order->contact = $card->address->contact;
                $order->contact_phone = $card->address->contact_phone;
                $order->customer_id = $card->customer_id;
                $order->customer_card_id = $card->id;
                $order->delivery_from = 630;
                $order->delivery_to = 930;
                $order->customer_address_id = $card->customer_address_id;
                $order->menu_id = $menuType->id;
                $order->save();
            }
        }
    }
}
