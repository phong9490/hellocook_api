<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerDishBlacklist extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dish_id',
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }

    public function dish()
    {
        return $this->belongsTo('App\Dish', 'dish_id');
    }
}
