<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerCard extends Model
{
    public function cardInfo()
    {
        return $this->belongsTo('App\Card', 'card_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }

    public function address()
    {
        return $this->belongsTo('App\CustomerAddress', 'customer_address_id');
    }
}
