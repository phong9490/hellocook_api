<?php

namespace App;

use App\OrderDetail;
use App\MenuDetail;
use App\CustomerCard;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const STATUS_ORDERED = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contact',
        'contact_phone',
        'delivery_date',
        'delivery_hour',
        'ward_id',
        'district_id',
        'province_id',
        'country_id',
    ];

    protected $appends = [
        'uuid',
    ];

    public static function boot()
    {
        parent::boot();
        self::created(function ($model) {
            $menuDetail = MenuDetail::where('menu_id', $model->menu_id)->get();
            foreach ($menuDetail as $detail) {
                $orderDetail = new OrderDetail();
                $orderDetail->order_id = $model->id;
                $orderDetail->dish_id = $detail->dish_id;
                $orderDetail->save();
            }

            $customerCard = CustomerCard::where('id', $model->customer_card_id)->first();
            if ($customerCard->order_count > 1) {
                $customerCard->order_count = $customerCard->order_count - 1;
                $customerCard->save();
            } else {
                $customerCard->order_count = 0;
                $customerCard->status = 0;
                $customerCard->save();
            }
        });


        self::updated(function ($model) {
            $original_status = $model->getOriginal('status');
            if ($original_status === 1 && $model->status === 0) {
                $customerCard = CustomerCard::where('id', $model->customer_card_id)->first();
                if ($customerCard->order_count > 0) {
                    $customerCard->order_count = $customerCard->order_count + 1;
                    $customerCard->save();
                } else {
                    $customerCard->order_count = 1;
                    $customerCard->status = 1;
                    $customerCard->save();
                }
            }
        });
    }

    public function getUuidAttribute()
    {
        return str_pad($this->id, 8, '0', STR_PAD_LEFT);
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }

    public function menu()
    {
        return $this->belongsTo('App\Menu', 'menu_id');
    }

    public function detail()
    {
        return $this->hasManyThrough('App\Dish', 'App\OrderDetail', 'order_id', 'id', 'id', 'dish_id')->addSelect("order_details.id as order_detail_id", "dishes.*");
    }

    public function detailReview()
    {
        return $this->hasManyThrough('App\OrderDetailReview', 'App\OrderDetail', 'order_id', 'order_detail_id', 'id', 'id')->addSelect("order_details.id as order_detail_id", "order_detail_reviews.*");
    }

    public function review()
    {
        return $this->hasOne('App\OrderReview');
    }

    public function delivery()
    {
        return $this->hasOne('App\CustomerAddress', 'id', 'customer_address_id');
    }
}
