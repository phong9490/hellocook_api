<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    public function dish()
    {
        return $this->belongsTo('App\Dish', 'dish_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }
}
