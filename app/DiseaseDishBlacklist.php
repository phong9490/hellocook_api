<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiseaseDishBlacklist extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dish_id',
    ];

    public function dish()
    {
        return $this->belongsTo('App\Dish', 'dish_id');
    }

    public function disease()
    {
        return $this->belongsTo('App\Disease', 'disease_id');
    }
}
