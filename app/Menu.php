<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
    ];

    public function details()
    {
        return $this->hasManyThrough('App\Dish', 'App\MenuDetail', 'menu_id', 'id', 'id', 'dish_id')->addSelect("menu_details.id as menu_detail_id", "dishes.*");
    }
}
