<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// non logged in route
Route::middleware('api')->group(function () {
    Route::post('/auth/login', 'AuthController@login');
    Route::post('/auth/login/cms', 'AuthController@loginCms');
    Route::post('/auth/register', 'AuthController@register');

    Route::get('/regions', 'RegionController@index');
    Route::get('/websites', 'WebsiteController@index');
    Route::get('/currencies', 'CurrencyController@index');
    Route::get('/websites/metadata', 'WebsiteController@getMetadata');
    Route::post('/files/upload', 'FileController@upload');
    Route::get('/menus/{day}', 'MenuController@menuByDay');
});

// logged in route
Route::middleware('auth:api')->group(function () {
    // profile route
    Route::prefix('profile')->group(function () {
        Route::put('/', 'ProfileController@update');
        Route::put('/change-password', 'ProfileController@changePassword');
        Route::put('/update', 'CustomerController@update');
    });

    // admin route
    Route::prefix('admin')->middleware('role.admin')->group(function () {
        Route::get('/users', 'UserController@index');
        Route::post('/users', 'UserController@store');
        Route::put('/users/{id}', 'UserController@update');
        Route::get('/addresses/{id}', 'CustomerAddressController@index');

        Route::get('/orders', 'OrderController@listOrders');
        Route::get('/orders/review', 'OrderController@listReviews');
        Route::post('/orders', 'OrderController@store');
        Route::put('/orders/{id}', 'OrderController@update');
        Route::get('/orders/{id}', 'OrderController@detail');
        Route::put('/orders/cancel/{id}', 'OrderController@cancelAdmin');

        Route::get('/cards', 'CardController@index');
        Route::put('/cards/{id}', 'CardController@update');
        Route::delete('/cards/{id}', 'CardController@destroy');
        Route::post('/cards', 'CardController@store');
        Route::get('/customercard/{id}', 'CardController@listById');

        Route::get('/dishes', 'DishController@index');
        Route::put('/dishes/{id}', 'DishController@update');
        Route::post('/dishes', 'DishController@store');
        Route::delete('/dish/{id}', 'DishController@destroy');

        Route::get('/menus', 'MenuController@index');
        Route::get('/menus/{day}', 'MenuController@menuByDay');
        Route::put('/menus/{id}', 'MenuController@update');
        Route::delete('/menu/{id}', 'MenuController@destroy');
        Route::post('/menus', 'MenuController@store');

        Route::get('/customers', 'CustomerController@index');
        Route::put('/customers/{id}', 'CustomerController@update');
        Route::get('/customer/{id}', 'CustomerController@addresses');
    });

    // order route
    Route::prefix('order')->middleware('role.customer')->group(function () {
        Route::get('/list', 'OrderController@index');
        Route::get('/ratings', 'OrderController@listRating');
        Route::post('/review', 'OrderController@review');
    });

    // customer route
    Route::prefix('customer')->middleware('role.customer')->group(function () {
        Route::get('/card', 'CustomerController@detailCard');
        Route::post('/card/register', 'CustomerController@registerCard');
        Route::get('/info', 'CustomerController@detail');
        Route::get('/orders', 'OrderController@indexCustomer');
        Route::put('/order/cancel/{id}', 'OrderController@cancelOrder');
        Route::get('/addresses', 'CustomerAddressController@index');
        Route::put('/addresses/{id}', 'CustomerAddressController@update');
        Route::post('/addresses', 'CustomerAddressController@store');
        Route::post('/changepassword', 'CustomerController@changePassword');
    });
});
